﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Diagnostics;
using System.IO;

namespace Lichten.SHPlugin.HighGamePriority
{
    
    [PluginName("HighGamePriority")]
    public class DataPluginDemo : IPlugin, IDataPlugin
    {

        /// <summary>
        /// Instance of the current plugin manager
        /// </summary>
        public PluginManager PluginManager { get; set; }

        private const int MAXCOUNT = 5000; //~1.3 min
        private int counter;

        /// <summary>
        /// Called at plugin manager stop, close/displose anything needed here !
        /// </summary>
        /// <param name="pluginManager"></param>
        public void End(PluginManager pluginManager)
        {
        }

        /// <summary>
        /// Called after plugins startup
        /// </summary>
        /// <param name="pluginManager"></param>
        public void Init(PluginManager pluginManager)
        {
            counter = MAXCOUNT;
        }

        /// <summary>
        /// Return your winform settings control here, return null if no settings control
        /// 
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <returns></returns>
        public System.Windows.Forms.Control GetSettingsControl(PluginManager pluginManager)
        {
            return null;
        }

        /// <summary>
        /// called one time per game data update
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <param name="data"></param>
        public void DataUpdate(PluginManager pluginManager, ref GameData data)
        {
            if (data.GameRunning && data.NewData != null)
            {
                //Saving time by checking irregularly
                if (counter < MAXCOUNT)
                {
                    counter++;
                    return;
                }

                Process pro = null;

                
                //Using get Process name
                if (pluginManager.GetProcesseNames() != null)
                {
                    var proNames = pluginManager.GetProcesseNames().GetEnumerator();

                    bool works = false;

                    try
                    {
                        works = proNames.MoveNext();
                    }
                    catch (InvalidOperationException)
                    {
                        works = false;
                    }
                    

                    if (works)
                    {
                        string name = proNames.Current;

                        if (name != null)
                            pro = getProcess(name);
                    }
                }

                //Fall back
                if (pro != null)
                {
                    pro = getProcess(data.GameName);

                    if (pro == null && data.GamePath != null)
                    {
                        string execName = Path.GetFileName(data.GamePath);
                        pro = getProcess(execName);

                        if (pro == null)
                        {
                            pro = getProcess(Path.GetFileNameWithoutExtension(execName));
                        }
                    }
                }

                //Gameprocess is not there yet, but the game counts as running, so next tick we will return
                if (pro == null)
                {
                    counter++;

                    if ((counter - (MAXCOUNT/4)) > MAXCOUNT)
                    {
                        //Simehub is acting strange, so we check back in MAXCOUNT x2
                        counter = 0 - MAXCOUNT;
                    }

                    return;
                }

                try
                {
                    if (pro.PriorityClass != ProcessPriorityClass.High)
                        pro.PriorityClass = ProcessPriorityClass.High;

                    if (!pro.PriorityBoostEnabled)
                        pro.PriorityBoostEnabled = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + "\n" + e.StackTrace);
                }
                finally
                {
                    counter = 0;
                }
            }
        }


        private Process getProcess(string processName)
        {
            Process[] pros = Process.GetProcessesByName(processName);

            if (pros == null || pros.Length == 0)
                return null;

            return pros[0];
        }

        
    }
}